<!DOCTYPE html>
<!--
Actividad a realizar 
Alumno @Cristobal_acuna
1.- Exponer Características y propiedades de IDE Netbeans.
2.- Desarrollo Programa usando Netbeans en los sigtes lenguajes:
Parte A PHP(nativo, sin framework) con HTML como presentación al usuario
(+javascript nativo sin framework para validaciones). 
Parte B Javascript nativo, sin framework con HTML para la presentación al usuario.
Problemática: Ingresar 2 nros y realizar operaciones básicas (+-*/) Tarea individual.
Presentación de mínimo 10min y máx 15min. Formato Universidad.

<!-- Comentar en HTML
/**/ Comentar en PHP
-->

<html>
    <head>
        <!-- Tipo de codificación-->
        <meta charset="UTF-8">
         <!-- Titulo que se despliega en el navegador-->
        <title>Operación de Números PHP</title>
    </head>
   <body>
       <H2> Operación de números</H2>
       <!-- Formulario creado en php-->
<form action="index.php" method="POST">
    <!-- Inicio de tabla-->
	<table>
            <!-- Fila 1-->
	<tr>
             <!-- Columna 1-->
		<td> Ingrese Nro 1: <input type="text" name="numero1"></td>
                 <!-- Columna 2-->
                <td> Ingrese Nro 2: <input type="text" name="numero2"></td>
                 <!-- Columna 3-->
               <td>  <input style="background-color: #FF9900"  type="submit" value="Calcular Operaciones (+,-,*,/)"> </td>
	</tr>
</table>
</form>
</body>
 <!-- Logica construida en php para operar con los números-->
<?php
	if($_POST)
	{	
		$num1 = $_POST
		['numero1'];
		$num2 = $_POST
		['numero2'];
                /* Función para sumar dos números*/
		$suma = $num1 
		+ $num2;
                  /* Función para restar dos números*/
                $resta= $num1 
                - $num2;
                  /* Función para multiplicar dos números*/
                $multi= $num1
                        * $num2;
                  /* Función para dividir dos números*/
                $divi= $num1
                        / $num2;
                  /* Función para imprimir las operaciones*/
                echo "Los Números ingresados son: ".$num1." y ".$num2;
                echo "<br>";
		echo "La Suma es ".$suma;
                echo "<br>";
                echo "La Resta es ".$resta;
                echo "<br>";
                echo "La Multiplicación es ".$multi;
                echo "<br>";
                echo "La División es " .$divi;
	}
?>
</html>
